﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MovingObject {

    public Text healthText;

    private Animator animator;
    private int playerHealth = 50;
    private int attackPower = 1;
    private int healthPerFruit = 5;
    private int healthPerSoda = 10;
    private int secondsUntilNextLevel = 1;

    protected override void Start()
    {
        base.Start();
        animator = GetComponent<Animator>();
        healthText.text = "Health: " + playerHealth;
    }

    void Update()
    {
        if (!GameController.Instance.isPlayerTurn)
        {
            return;
        }

        int xAxis = 0;
        int yAxis = 0;

        xAxis = (int)Input.GetAxisRaw("Horizontal");
        yAxis = (int)Input.GetAxisRaw("Vertical");

        if (xAxis != 0)
        {
            yAxis = 0;
        }

        if (xAxis != 0 || yAxis != 0)
        {
            playerHealth--;
            healthText.text = "Health: " + playerHealth;
            Move<Wall>(xAxis, yAxis);
            GameController.Instance.isPlayerTurn = false;
        }
    }

    private void onTriggerEnter2D(Collider2D objectPlayerCollideWith)
    {
        if(objectPlayerCollideWith.tag == "Exit")
        {
            Invoke("LoadNewLevel1", secondsUntilNextLevel);
        }
        else if(objectPlayerCollideWith.tag == "Fruit")
        {
            playerHealth += healthPerFruit;
            healthText.text = "+" + healthPerFruit + " Health\n" + "Health: " + playerHealth;
            objectPlayerCollideWith.gameObject.SetActive(false);
        }
        else if(objectPlayerCollideWith.tag == "Soda")
        {

            playerHealth += healthPerSoda;
            healthText.text = "+" + healthPerSoda + " Health\n" + "Health: " + playerHealth;
            objectPlayerCollideWith.gameObject.SetActive(false);

        }
    }

    private void LoadNewLevel1()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    protected override void HandleCollision<T>(T component)
    {

        Wall wall = component as Wall;
        animator.SetTrigger("playerAttack");
        wall.DamageWall(attackPower);
    }

    public void TakenDamage(int damageRecived)
    {
        playerHealth -= damageRecived;
        healthText.text = "-" + damageRecived + "Health\n" + "Health " + playerHealth;
        animator.SetTrigger("playerHurt");
    } 
}

